# Overview
This repo contains an API that can be used to keep up with a My Little Pony Collection. (What, you don't have one?) Users can create/update/delete colors, pony types (e.g., Unicorn, Pegasus, etc.), and ponies. To

This branch uses the following technologies:

* .NET Core Web API
* Entity Framework (EF)
* XUnit
* Moq
* MySQL

## A note on the EF implementation

I prefer to use bare SQL when possible because "opinionated" ORMs like EF and NHibernate can create performance and maintenance nightmares. (Dapper, however, works well.) I used EF in this project simply to become familiar with it (unit testing strategies, design patterns, etc.).

The Entity Framework piece is encapsulated using the Repository / Unit of Work pattern as laid out by Microsoft here: https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application

There are some problems with this implementation, as described here (and elsewhere):
https://lostechies.com/derekgreer/2015/11/01/survey-of-entity-framework-unit-of-work-patterns/ 

# Prerequisites

* Install .NET Core > 2.0: https://www.microsoft.com/net/download
* Install Visual Studio Community (https://visualstudio.microsoft.com/downloads/) or Visual Studio Code (https://code.visualstudio.com/download)
* Install MySql Server 5.7 Installer: https://dev.mysql.com/downloads/mysql/5.7.html#downloads. 
	* When you run the installer, you'll need to install both MySql Server 5.7 and MySql Workbench.
	* When prompted, set your root password to "password"
	* When the installation is complete, open MySql Workbench, and log in:
		* Server: 127.0.0.1
		* Port: 3306
		* User: root
		* Password: password
		* Database: sys
	* Run the commands in the database.sql file at the root of this repo.
* Postman (for interacting with the API): https://www.getpostman.com/apps
	
# Running the Unit Tests (Command Line)
```
cd /PonyCollection
dotnet restore
cd PonyCollection.Tests
dotnet test
```

# Running the API (Command Line)
```
cd /PonyCollection
dotnet restore
cd PonyCollection
dotnet run
```

# Debugging the API (VS Code)

* Open the solution folder in VS Code.
* Click the Debug tab.
* Click the Play button. (If you have problems with this, you may need to adjust the settings in the launch.json and tasks.json files.)