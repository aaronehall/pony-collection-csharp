CREATE DATABASE IF NOT EXISTS PonyCollection;

USE PonyCollection;

-- ****************

DROP TABLE IF EXISTS `Pony`;

CREATE TABLE `Pony` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(36) NOT NULL DEFAULT '',
  `bodyColorId` int(11) NOT NULL,
  `maneColorId` int(11) NOT NULL,
  `ponyTypeId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX uc_file ON `Pony` (`id`);

-- ****************

DROP TABLE IF EXISTS `Color`;

CREATE TABLE `Color` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_file` (`id`),
  UNIQUE KEY `uc_Name` (`Name`)
);

-- ****************

DROP TABLE IF EXISTS `PonyType`;

CREATE TABLE `PonyType` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_file` (`id`),
  UNIQUE KEY `uc_Name` (`name`)
);

-- ****************
INSERT INTO `Color` (name)
VALUES 
  ('red')
  ,('orange')
  ,('yellow')
  ,('green')
  ,('blue')
  ,('purple')
  ,('black')
  ,('white')
  ,('brown')
  ,('pink');

-- ****************
INSERT INTO `PonyType` (name)
VALUES 
  ('EarthPony')
  ,('Unicorn')
  ,('Pegasus')
  ,('Alicorn');

-- ****************

INSERT INTO `Pony` (name, bodyColorId, maneColorId, ponyTypeId)
VALUES 
('Twilight Sparkle', 6, 6, 2)
,('Rarity', 8, 6, 2)
,('Applejack', 2, 3, 1)
,('Pinkie Pie', 10, 10, 1)
,('Fluttershy', 3, 10, 3);
