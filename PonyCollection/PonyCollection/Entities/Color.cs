﻿using System.ComponentModel.DataAnnotations;

namespace PonyCollection.Entities
{
    public class Color : EntityBase
    {
        [Required(ErrorMessage = "Your color needs a name.")]
        [StringLength(20, ErrorMessage = "Your color's name can't be longer than 20 characters.")]
        public override string Name { get; set; }       
    }
}
