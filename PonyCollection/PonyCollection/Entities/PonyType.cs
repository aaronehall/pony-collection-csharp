﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace PonyCollection.Entities
{
    public class PonyType : EntityBase
    {
        [Required(ErrorMessage = "Your pony type needs a name.")]
        [StringLength(10, ErrorMessage = "Your pony type's name can't be longer than 10 characters.")]
        public override string Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<Pony> Ponies { get; set; }
    }
}
