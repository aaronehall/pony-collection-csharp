﻿using Microsoft.EntityFrameworkCore;
using PonyCollection.Models;

namespace PonyCollection.Entities
{
    public class PonyCollectionDbContext : DbContext
    {
        // TODO: Better organize namespaces
        public virtual DbSet<Pony> Pony { get; set; } 
        public virtual DbSet<Color> Color { get; set; }
        public virtual DbSet<PonyType> PonyType { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("Server=127.0.0.1;Database=PonyCollection;User=root;Pwd=password;Port=3306;SslMode=none");
        }
    }
}
