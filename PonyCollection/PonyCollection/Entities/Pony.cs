﻿namespace PonyCollection.Entities
{
    public class Pony : EntityBase
    {
        public int BodyColorId { get; set; }
        public int ManeColorId { get; set; }
        public int PonyTypeId { get; set; }

        public PonyType PonyType { get; set; }
        public Color BodyColor { get; set; }
        public Color ManeColor { get; set; }
    }
}
