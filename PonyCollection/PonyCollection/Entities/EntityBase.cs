﻿namespace PonyCollection.Entities
{
    public class EntityBase
    {
        public int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
