﻿using System.ComponentModel.DataAnnotations;

namespace PonyCollection.Models
{
    public class PonyModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Your pony needs a name.")]
        [StringLength(36, ErrorMessage = "Your pony's name can't be longer than 36 characters.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Your pony needs a body color.")]
        [StringLength(20, ErrorMessage = "Your pony's body color can't be longer than 20 characters.")]
        public string BodyColor { get; set; }

        [Required(ErrorMessage = "Your pony needs a mane color.")]
        [StringLength(20, ErrorMessage = "Your pony's mane color can't be longer than 20 characters.")]
        public string ManeColor { get; set; }

        [Required(ErrorMessage = "Your pony needs a pony type.")]
        [StringLength(10, ErrorMessage = "Your pony's type can't be longer than 10 characters.")]
        public string PonyType { get; set; }
    }
}
