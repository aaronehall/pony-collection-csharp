﻿using System;
using PonyCollection.Entities;

namespace PonyCollection.Repositories
{
    public class UnitOfWork : IDisposable
    {
        // TODO: Won't this become a "god class"?
        // Implemented as described in
        // https://www.youtube.com/watch?v=rtXpYpZdOzM&t=904s and 
        // https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application#creating-the-unit-of-work-class
        // https://medium.com/@mlbors/using-the-repository-pattern-with-the-entity-framework-fa4679f2139
        private readonly PonyCollectionDbContext _database;
        private RepositoryBase<Pony> _poniesRepository;
        private RepositoryBase<Color> _colorsRepository;
        private RepositoryBase<PonyType> _ponyTypesRepository;
        private PonyModelToDbTranslator _translator;

        public UnitOfWork(PonyCollectionDbContext database)
        {
            _database = database;
            _translator = new PonyModelToDbTranslator(_database);
            _poniesRepository = new RepositoryBase<Pony>(_database);
            _colorsRepository = new RepositoryBase<Color>(_database);
            _ponyTypesRepository = new RepositoryBase<PonyType>(_database);
        }

        public RepositoryBase<Pony> PoniesRepository
        {
            get
            {
                if (_poniesRepository == null)
                {
                    _poniesRepository = new RepositoryBase<Pony>(_database);
                }

                return _poniesRepository;
            }
        }

        public RepositoryBase<Color> ColorsRepository
        {
            get
            {
                if (_colorsRepository == null)
                {
                    _colorsRepository = new RepositoryBase<Color>(_database);
                }

                return _colorsRepository;
            }
        }

        public RepositoryBase<PonyType> PonyTypesRepository
        {
            get
            {
                if (_ponyTypesRepository == null)
                {
                    _ponyTypesRepository = new RepositoryBase<PonyType>(_database);
                }

                return _ponyTypesRepository;
            }
        }

        public PonyModelToDbTranslator Translator
        {
            get
            {
                if (_translator == null)
                {
                    _translator = new PonyModelToDbTranslator(_database);
                }

                return _translator;
            }
        }

        public virtual void Complete()
        {
            _database.SaveChanges();
        }

        public virtual void Dispose()
        {
            _database.Dispose();
        }
    }
}
