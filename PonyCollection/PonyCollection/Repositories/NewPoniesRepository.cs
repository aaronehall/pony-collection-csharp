﻿using PonyCollection.Entities;

namespace PonyCollection.Repositories
{
    public class NewPoniesRepository : RepositoryBase<Pony>
    {
        public NewPoniesRepository(PonyCollectionDbContext database) : base(database)
        {
        }
    }
}
