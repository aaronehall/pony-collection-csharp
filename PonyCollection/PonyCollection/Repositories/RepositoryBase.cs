﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using PonyCollection.Entities;

namespace PonyCollection.Repositories
{
    //http://www.tugberkugurlu.com/archive/generic-repository-pattern-entity-framework-asp-net-mvc-and-unit-testing-triangle
    // OR https://blog.goyello.com/2016/07/14/save-time-mocking-use-your-real-entity-framework-dbcontext-in-unit-tests/
    public class RepositoryBase<T> where T : EntityBase
    {
        private readonly PonyCollectionDbContext _database;

        public RepositoryBase(PonyCollectionDbContext database)
        {
            _database = database;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _database.Set<T>().ToList();
        }

        public virtual T Get(Expression<Func<T, bool>> predicate)
        {
            return _database
                .Set<T>()
                .Where(predicate)
                .FirstOrDefault();
        }

        public virtual void Add(T entity)
        {
            _database.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            _database.Set<T>().Remove(entity);
        }
    }
}
