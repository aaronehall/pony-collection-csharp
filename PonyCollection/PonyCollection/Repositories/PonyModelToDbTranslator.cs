﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PonyCollection.Entities;
using PonyCollection.Models;

namespace PonyCollection.Repositories
{
    public class PonyModelToDbTranslator
    {
        private PonyCollectionDbContext _dbContext;

        public PonyModelToDbTranslator(PonyCollectionDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public PonyModel HydratePonyModel(Pony ponyDbEntity, PonyModel ponyModel)
        {
            ponyModel.Id = ponyDbEntity.Id;
            ponyModel.Name = ponyDbEntity.Name;
            ponyModel.BodyColor = ponyDbEntity.BodyColor.Name;
            ponyModel.ManeColor = ponyDbEntity.ManeColor.Name;
            ponyModel.PonyType = ponyDbEntity.PonyType.Name;

            return ponyModel;
        }

        public PonyModel GetModelFromDbEntity(Pony ponyDbEntity)
        {
            return _dbContext.Pony
                             .Include(ponyEntity => ponyEntity.PonyType)
                             .Include(ponyEntity => ponyEntity.ManeColor)
                             .Include(ponyEntity => ponyEntity.BodyColor)
                             .Select(ponyEntity => new PonyModel
                             {
                                 Id = ponyEntity.Id,
                                 Name = ponyEntity.Name,
                                 BodyColor = ponyEntity.BodyColor.Name,
                                 ManeColor = ponyEntity.ManeColor.Name,
                                 PonyType = ponyEntity.PonyType.Name
                             })
                             .FirstOrDefault(ponyEntity => ponyEntity.Id == ponyDbEntity.Id);
        }

        public List<PonyModel> GetModelsFromDbEntities(IEnumerable<Pony> ponyDbEntities)
        {
            var ponyModels = new List<PonyModel>();

            foreach (var ponyDbEntity in ponyDbEntities)
            {
                ponyModels.Add(GetModelFromDbEntity(ponyDbEntity));
            }

            return ponyModels;
        }

        public Pony GetDbEntityFromModel(PonyModel ponyModel)
        {
            return new Pony
            {
                Id = ponyModel.Id,
                Name = ponyModel.Name,
                BodyColorId = _dbContext.Color.FirstOrDefault(c => c.Name == ponyModel.BodyColor).Id,
                ManeColorId = _dbContext.Color.FirstOrDefault(c => c.Name == ponyModel.ManeColor).Id,
                PonyTypeId = _dbContext.PonyType.FirstOrDefault(t => t.Name == ponyModel.PonyType).Id
            };
        }

        public List<Pony> GetDbEntitiesFromModels(IEnumerable<PonyModel> ponyModels)
        {
            var ponyDbEntities = new List<Pony>();

            foreach (var ponyModel in ponyModels)
            {
                ponyDbEntities.Add(GetDbEntityFromModel(ponyModel));
            }

            return ponyDbEntities;
        }
    }
}
