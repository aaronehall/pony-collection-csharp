﻿using System;
using Microsoft.AspNetCore.Mvc;
using PonyCollection.Entities;
using PonyCollection.Models;
using PonyCollection.Repositories;

namespace PonyCollection.Controllers
{
    [Route("api/[controller]")]
    public class PoniesController : Controller, IDisposable
    {
        private readonly UnitOfWork _unitOfWork;

        public PoniesController() : this(new UnitOfWork(new PonyCollectionDbContext())) { }

        internal PoniesController(UnitOfWork unitOfWork) 
        {
            _unitOfWork = unitOfWork;
        }

        /// <returns>An array of all ponies.</returns>
        [HttpGet]
        public IActionResult Get()
        {
            var ponyDbEntities = _unitOfWork.PoniesRepository.GetAll();

            var ponyModels = _unitOfWork.Translator.GetModelsFromDbEntities(ponyDbEntities);

            return new JsonResult(ponyModels) { StatusCode = 200 };
        }

        /// <param name="id"></param>
        /// <returns>The pony with the specified ID.</returns>
        [ProducesResponseType(404)]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var ponyDbEntity = _unitOfWork.PoniesRepository.Get(p => p.Id == id);

            if (ponyDbEntity == null) 
            {
                return new NotFoundResult();   
            }

            var ponyModel = _unitOfWork.Translator.GetModelFromDbEntity(ponyDbEntity);

            return new JsonResult(ponyModel) { StatusCode = 200 };
        }

        /// <param name="id"></param>
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
            var ponyToDelete = _unitOfWork.PoniesRepository.Get(p => p.Id == id);
			
            if (ponyToDelete == null)
			{
				return new NotFoundResult();
			}
			
            _unitOfWork.PoniesRepository.Delete(ponyToDelete);
            _unitOfWork.Complete();
			
			return new NoContentResult();
		}

        /// <param name="ponyToCreate"></param>
        /// <returns>The newly created pony.</returns>
        /// <response code="400">If the new pony is missing or has a missing color or pony type.</response>
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public IActionResult Post([FromBody]PonyModel ponyToCreate)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }

            var bodyColor = _unitOfWork.ColorsRepository.Get(color => color.Name == ponyToCreate.BodyColor);
            if (bodyColor == null)
            {
                return new JsonResult("Your pony's body color doesn't exist!") { StatusCode = 400 };
            }

            var maneColor = _unitOfWork.ColorsRepository.Get(color => color.Name == ponyToCreate.ManeColor);
            if (maneColor == null)
            {
                return new JsonResult("Your pony's mane color doesn't exist!") { StatusCode = 400 };
            }

            var ponyType = _unitOfWork.PonyTypesRepository.Get(pt => pt.Name == ponyToCreate.PonyType);
            if (ponyType == null)
            {
                return new JsonResult("Your pony's pony type doesn't exist!") { StatusCode = 400 };
            }

            var ponyDbEntity = new Pony
            {
                Name = ponyToCreate.Name,
                BodyColor = bodyColor,
                BodyColorId = bodyColor.Id,
                ManeColor = maneColor,
                ManeColorId = maneColor.Id,
                PonyType = ponyType,
                PonyTypeId = ponyType.Id
            };

            _unitOfWork.PoniesRepository.Add(ponyDbEntity);
            _unitOfWork.Complete();

            ponyToCreate.Id = ponyDbEntity.Id;

            return new JsonResult(ponyToCreate) { StatusCode = 201 };
        }

        /// <param name="id"></param>
        /// <param name="ponyToUpdate"></param>
        /// <returns>The updated pony.</returns>
        /// <response code="400">If the updated pony is missing or has a missing color or pony type.</response>
        [ProducesResponseType(202)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]PonyModel ponyToUpdate)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }
            
            var existingPonyDbEntity = _unitOfWork.PoniesRepository.Get(p => p.Id == id);
            if (existingPonyDbEntity == null)
            {
                return new NotFoundResult();
            }

            var bodyColor = _unitOfWork.ColorsRepository.Get(color => color.Name == ponyToUpdate.BodyColor);
            ponyToUpdate.Id = id;

            if (bodyColor == null)
			{
				return new JsonResult("Your pony's body color doesn't exist!") { StatusCode = 400 };
			}
			
            var maneColor = _unitOfWork.ColorsRepository.Get(color => color.Name == ponyToUpdate.ManeColor);
            if (maneColor == null)
			{
				return new JsonResult("Your pony's mane color doesn't exist!") { StatusCode = 400 };
			}

            var ponyType = _unitOfWork.PonyTypesRepository.Get(pt => pt.Name == ponyToUpdate.PonyType);
            if (ponyType == null)
            {
                return new JsonResult("Your pony's pony type doesn't exist!") { StatusCode = 400 };
            }

            existingPonyDbEntity.Name = ponyToUpdate.Name;
            existingPonyDbEntity.BodyColorId = bodyColor.Id;
            existingPonyDbEntity.ManeColorId = maneColor.Id;
            existingPonyDbEntity.PonyTypeId = ponyType.Id;

            _unitOfWork.Complete();

            return new JsonResult(ponyToUpdate) { StatusCode = 202 };
        }

		protected override void Dispose(bool disposing)
		{
            _unitOfWork.Dispose();
            base.Dispose(disposing);
		}
	}
}
