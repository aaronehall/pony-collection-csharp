﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PonyCollection.Entities;
using PonyCollection.Repositories;

namespace PonyCollection.Controllers
{
    [Route("api/[controller]")]
	public class PonyTypesController : Controller, IDisposable
    {
        private readonly UnitOfWork _unitOfWork;

        public PonyTypesController() : this(new UnitOfWork(new PonyCollectionDbContext())) { }

        internal PonyTypesController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <returns>An array of all pony types.</returns>
        [HttpGet]
        public IActionResult Get()
        {
            var ponyTypes =_unitOfWork.PonyTypesRepository.GetAll();

            return new JsonResult(ponyTypes) { StatusCode = 200 };
        }

        /// <param name="id"></param>
        /// <returns>The pony type with the specified ID.</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(404)]
        public IActionResult Get(int id)
        {
            var ponyType = _unitOfWork.PonyTypesRepository.Get(pt => pt.Id == id);

            if (ponyType == null) return new NotFoundResult();

            return new JsonResult(ponyType) { StatusCode = 200 };
        }

        /// <param name="id"></param>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult Delete(int id)
        {
            var ponyType = _unitOfWork.PonyTypesRepository.Get(pt => pt.Id == id);

            if (ponyType == null) return new NotFoundResult();

            _unitOfWork.PonyTypesRepository.Delete(ponyType);
            _unitOfWork.Complete();

            return new NoContentResult();
        }

        /// <param name="ponyTypeToCreate"></param>
        /// <returns>The newly created pony type.</returns>
        /// <response code="400">If the new pony type is missing or already exists.</response>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public IActionResult Post([FromBody]PonyType ponyTypeToCreate)
        {
            if (!ModelState.IsValid) return new BadRequestObjectResult(ModelState);

            _unitOfWork.PonyTypesRepository.Add(ponyTypeToCreate);

            try
            {
                _unitOfWork.Complete();
            }
            catch(DbUpdateException exception)
            {
                if (exception.InnerException.Message == $"Duplicate entry '{ponyTypeToCreate.Name}' for key 'uc_Name'")
                {
                    return new JsonResult("Oops! That pony type already exists.") { StatusCode = 400 };
                }
            }

            return new JsonResult(ponyTypeToCreate) { StatusCode = 201 };
        }

        /// <param name="ponyTypeToUpdate"></param>
        /// <param name="id"></param>
        /// <returns>The updated pony type.</returns>
        /// <response code="400">If the updated pony type is missing or already exists.</response>
        [ProducesResponseType(202)]
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]PonyType ponyTypeToUpdate)
        {
            if (!ModelState.IsValid) return new BadRequestObjectResult(ModelState);

            var existingPonyType = _unitOfWork.PonyTypesRepository.Get(pt => pt.Id == id);

            if (existingPonyType == null) return new NotFoundResult();

            existingPonyType.Name = ponyTypeToUpdate.Name;

            try
            {
                _unitOfWork.Complete();
            }
            catch(DbUpdateException exception)
            {
                if (exception.InnerException.Message == $"Duplicate entry '{ponyTypeToUpdate.Name}' for key 'uc_Name'")
                {
                    return new JsonResult("Oops! That pony type already exists.") { StatusCode = 400 };
                }
            }

            return new JsonResult(existingPonyType) { StatusCode = 202 };
        }

        protected override void Dispose(bool disposing)
        {
			_unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
