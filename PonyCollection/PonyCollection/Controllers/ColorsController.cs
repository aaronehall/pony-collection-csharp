﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PonyCollection.Entities;
using PonyCollection.Repositories;

namespace PonyCollection.Controllers
{
    [Route("api/[controller]")]
    public class ColorsController : Controller, IDisposable
    {
        private readonly UnitOfWork _unitOfWork;

        internal ColorsController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ColorsController() : this(new UnitOfWork(new PonyCollectionDbContext())) { }

        /// <returns>An array of all colors.</returns>
        [HttpGet]
        public IActionResult Get()
        {
            var colors = _unitOfWork.ColorsRepository.GetAll();

            return new JsonResult(colors) { StatusCode = 200 };
        }

        /// <param name="id"></param>
        /// <returns>The color with the specified ID.</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(404)]
        public IActionResult Get(int id)
        {
            var color = _unitOfWork.ColorsRepository.Get(c => c.Id == id);

            if (color == null) return new NotFoundResult();

            return new JsonResult(color) { StatusCode = 200 };
        }

        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult Delete(int id)
        {
            var colorToDelete = _unitOfWork.ColorsRepository.Get(c => c.Id == id);

            if (colorToDelete == null) return new NotFoundResult();

            _unitOfWork.ColorsRepository.Delete(colorToDelete);
            _unitOfWork.Complete();

            return new NoContentResult();
        }

        /// <param name="colorToCreate"></param>
        /// <returns>The newly created color.</returns>
        /// <response code="400">If the new color is missing or already exists.</response>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public IActionResult Post([FromBody]Color colorToCreate)
        {
            if (!ModelState.IsValid) return new BadRequestObjectResult(ModelState);

            _unitOfWork.ColorsRepository.Add(colorToCreate);

            try
            {
                _unitOfWork.Complete();
            }
            catch (DbUpdateException exception)
            {
                if (exception.InnerException.Message == $"Duplicate entry '{colorToCreate.Name}' for key 'uc_Name'")
                {
                    return new JsonResult("Oops! That color already exists.") { StatusCode = 400 };
                }
            }

            return new JsonResult(colorToCreate) { StatusCode = 201 };
        }

        /// <param name="id"></param>
        /// <param name="colorToUpdate"></param>
        /// <returns>The newly updated color.</returns>
        /// <response code="400">If the updated color is missing or already exists.</response>
        [HttpPut("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(202)]
        public IActionResult Put(int id, [FromBody]Color colorToUpdate)
        {
            if (!ModelState.IsValid) return new BadRequestObjectResult(ModelState);

            var existingColor = _unitOfWork.ColorsRepository.Get(c => c.Id == id);

            if (existingColor == null) return new NotFoundResult();

            existingColor.Name = colorToUpdate.Name;

            try
            {
                _unitOfWork.Complete();
            }
            catch (DbUpdateException exception)
            {
                if (exception.InnerException.Message == $"Duplicate entry '{colorToUpdate.Name}' for key 'uc_Name'")
                {
                    return new JsonResult("Oops! That color already exists.") { StatusCode = 400 };
                }
            }

            return new JsonResult(existingColor) { StatusCode = 202 };
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
	}
}
