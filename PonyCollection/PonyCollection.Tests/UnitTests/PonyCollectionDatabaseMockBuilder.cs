﻿using Microsoft.EntityFrameworkCore;
using Moq;
using PonyCollection.Entities;

namespace PonyCollection.Tests.UnitTests
{
    public class PonyCollectionDatabaseMockBuilder
    {
        private readonly Mock<PonyCollectionDbContext> _databaseMock = new Mock<PonyCollectionDbContext>();
        private DbSet<Pony> _ponyTable;
        private DbSet<Color> _colorTable;
        private DbSet<PonyType> _ponyTypeTable;

        public PonyCollectionDatabaseMockBuilder WithPonyTable(DbSet<Pony> ponyTable)
        {
            _ponyTable = ponyTable;
            return this;
        }

        public PonyCollectionDatabaseMockBuilder WithColorTable(DbSet<Color> colorTable)
        {
            _colorTable = colorTable;
            return this;
        }

        public PonyCollectionDatabaseMockBuilder WithPonyTypeTable(DbSet<PonyType> ponyTypeTable)
        {
            _ponyTypeTable = ponyTypeTable;
            return this;
        }

        public Mock<PonyCollectionDbContext> Build()
        {
            if (_ponyTable != null)
            {
                _databaseMock.Setup(db => db.Pony).Returns(_ponyTable);
                _databaseMock.Setup(db => db.Set<Pony>()).Returns(_ponyTable);
            }

            if (_colorTable != null)
            {
                _databaseMock.Setup(db => db.Color).Returns(_colorTable);
                _databaseMock.Setup(db => db.Set<Color>()).Returns(_colorTable);
            }

            if (_ponyTypeTable != null)
            {
                _databaseMock.Setup(db => db.PonyType).Returns(_ponyTypeTable);
                _databaseMock.Setup(db => db.Set<PonyType>()).Returns(_ponyTypeTable);
            }

            return _databaseMock;
        }
    }
}
