﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using PonyCollection.Controllers;
using PonyCollection.Entities;
using PonyCollection.Models;
using PonyCollection.Repositories;
using Xunit;

namespace PonyCollection.Tests.UnitTests
{
    public class PoniesController_should_
    {
        [Fact]
        public void retrieve_all_ponies()
        {
			var expectedPonies = Any.PonyDbEntities(3);

            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(expectedPonies)
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);
            
            var response = new PoniesController(unitOfWork).Get() as JsonResult;
            var actualPonies = response.Value as List<PonyModel>;

            Assert.Equal((int)HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(expectedPonies[0].Name, actualPonies[0].Name);
            Assert.Equal(expectedPonies[1].Name, actualPonies[1].Name);
            Assert.Equal(expectedPonies[2].Name, actualPonies[2].Name);
        }

        [Fact]
        public void return_an_empty_list_if_no_ponies_are_found_when_attempting_to_retrieve_all_ponies()
        {
            var emptyListOfPonyDbEntities = new List<Pony>();

            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(emptyListOfPonyDbEntities)
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);
            
            var response = new PoniesController(unitOfWork).Get() as JsonResult;
            var actualPonies = response.Value as List<PonyModel>;

            Assert.Equal((int)HttpStatusCode.OK, response.StatusCode);
            Assert.Empty(actualPonies);
        }

        [Fact]
        public void retrieve_one_pony()
        {
            var expectedPonyDbEntity = Any.PonyDbEntity();

            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(new List<Pony> { expectedPonyDbEntity })
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWork).Get(expectedPonyDbEntity.Id) as JsonResult;
            var actualPony = response.Value as PonyModel;

            Assert.Equal((int)HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(expectedPonyDbEntity.Id, actualPony.Id);
            Assert.Equal(expectedPonyDbEntity.Name, actualPony.Name);
            Assert.Equal(expectedPonyDbEntity.BodyColor.Name, actualPony.BodyColor);
            Assert.Equal(expectedPonyDbEntity.ManeColor.Name, actualPony.ManeColor);
            Assert.Equal(expectedPonyDbEntity.PonyType.Name, actualPony.PonyType);
        }

        [Fact]
        public void error_when_attempting_to_retrieve_a_pony_that_does_not_exist()
        {
            var emptyListOfPonyDbEntities = new List<Pony>();

            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(emptyListOfPonyDbEntities)
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);
            
            var response = new PoniesController(unitOfWork).Get(Any.Int());

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public void delete_a_pony()
        {
            var ponyToDelete = Any.PonyDbEntity();

            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(new List<Pony> { ponyToDelete })
                .WithEntityToDelete(ponyToDelete)
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWork).Delete(ponyToDelete.Id);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void error_when_attempting_to_delete_a_pony_that_does_not_exist()
        {
            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(new List<Pony>())
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWork).Delete(Any.Int());

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public void create_a_pony()
        {
            var expectedBodyColor = Any.Color();
            var expectedManeColor = Any.Color();
            var expectedPonyType = Any.PonyType();

            var ponyModelToCreate = new PonyModel
            {
                Id = Any.Int(),
                Name = Any.String(),
                PonyType = expectedPonyType.Name,
                BodyColor = expectedBodyColor.Name,
                ManeColor = expectedManeColor.Name
            };

            var ponyDbEntityToCreate = new Pony
            {
                Id = ponyModelToCreate.Id,
                Name = ponyModelToCreate.Name,
                BodyColorId = expectedBodyColor.Id,
                ManeColorId = expectedManeColor.Id,
                PonyTypeId = expectedPonyType.Id,
                PonyType = expectedPonyType,
                BodyColor = expectedBodyColor,
                ManeColor = expectedManeColor
            };

            var ponyTableStub = new TableMockBuilder<Pony>()
                .Build()
                .Object;

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color> { expectedBodyColor, expectedManeColor })
                .Build()
                .Object;

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType> { expectedPonyType })
                .Build()
                .Object;
            
            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .WithColorTable(colorTableStub)
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWorkStub = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWorkStub).Post(ponyModelToCreate) as JsonResult;
            var actualPony = response.Value as PonyModel;

            Assert.Equal((int)HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(ponyModelToCreate.Name, actualPony.Name);
            Assert.Equal(ponyModelToCreate.BodyColor, actualPony.BodyColor);
            Assert.Equal(ponyModelToCreate.ManeColor, actualPony.ManeColor);
            Assert.Equal(ponyModelToCreate.PonyType, actualPony.PonyType);
        }

        [Fact]
        public void update_a_pony()
        {
            var expectedBodyColor = Any.Color();
            var expectedManeColor = Any.Color();
            var expectedPonyType = Any.PonyType();

            var existingPonyDbEntity = new Pony
            {
                Id = Any.Int(),
                Name = Any.String(),
                BodyColorId = expectedBodyColor.Id,
                ManeColorId = expectedManeColor.Id,
                PonyTypeId = expectedPonyType.Id,
                PonyType = expectedPonyType,
                BodyColor = expectedBodyColor,
                ManeColor = expectedManeColor
            };

            var updatedPonyModel = new PonyModel
            {
                Id = existingPonyDbEntity.Id,
                Name = Any.String(),
                PonyType = expectedPonyType.Name,
                BodyColor = expectedBodyColor.Name,
                ManeColor = expectedManeColor.Name
            };

            var updatedPonyDbEntity = new Pony
            {
                Id = updatedPonyModel.Id,
                Name = updatedPonyModel.Name,
                BodyColorId = expectedBodyColor.Id,
                ManeColorId = expectedManeColor.Id,
                PonyTypeId = expectedPonyType.Id,
                PonyType = expectedPonyType,
                BodyColor = expectedBodyColor,
                ManeColor = expectedManeColor
            };

            var poniesTableStubAfterUpdate = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(new List<Pony> { updatedPonyDbEntity })
                .Build()
                .Object;

            var colorsTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color> { expectedBodyColor, expectedManeColor })
                .Build()
                .Object;

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType> { expectedPonyType })
                .Build()
                .Object;
            
            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorsTableStub)
                .WithPonyTypeTable(ponyTypeTableStub)
                .WithPonyTable(poniesTableStubAfterUpdate)
                .Build()
                .Object;

            var unitOfWorkStub = new UnitOfWorkStubBuilder()
                .WithPonyCollectionDbContext(databaseStub)
                .Build();

            var response = new PoniesController(unitOfWorkStub).Put(updatedPonyModel.Id, updatedPonyModel) as JsonResult;
            var actualPony = response.Value as PonyModel;

            Assert.Equal((int)HttpStatusCode.Accepted, response.StatusCode);
            Assert.Equal(updatedPonyModel.Name, actualPony.Name);
            Assert.Equal(updatedPonyModel.BodyColor, actualPony.BodyColor);
            Assert.Equal(updatedPonyModel.ManeColor, actualPony.ManeColor);
            Assert.Equal(updatedPonyModel.PonyType, actualPony.PonyType);
        }

        [Fact]
        public void error_when_attempting_to_update_a_pony_that_does_not_exist()
        {
            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(new List<Pony>())
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWork).Put(Any.Int(), Any.PonyModel());

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public void return_bad_request_error_when_new_pony_is_invalid_for_any_reason()
        {
            var expectedErrorKey = Any.String();
            var expectedErrorMessage = Any.String();

            var poniesController = new PoniesController(new UnitOfWork(new PonyCollectionDatabaseMockBuilder().Build().Object));

            poniesController.ModelState.AddModelError(expectedErrorKey, expectedErrorMessage);

            var response = poniesController.Post(null) as BadRequestObjectResult;
            var errors = response.Value as SerializableError;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal(expectedErrorKey, errors.Keys.First());
            Assert.Equal(new [] { expectedErrorMessage }, errors.Values.First());
        }

        [Fact]
        public void return_bad_request_error_when_updated_pony_is_invalid_for_any_reason()
        {
            var expectedErrorKey = Any.String();
            var expectedErrorMessage = Any.String();

            var poniesController = new PoniesController(new UnitOfWork(new PonyCollectionDatabaseMockBuilder().Build().Object));

            poniesController.ModelState.AddModelError(expectedErrorKey, expectedErrorMessage);

            var response = poniesController.Put(Any.Int(), null) as BadRequestObjectResult;
            var errors = response.Value as SerializableError;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal(expectedErrorKey, errors.Keys.First());
            Assert.Equal(new[] { expectedErrorMessage }, errors.Values.First());
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_without_name()
        {
            var ponyWithMissingName = new PonyModel
            {
                BodyColor = Any.String(),
                ManeColor = Any.String(),
                PonyType = Any.String()
            };

            var validationContext = new ValidationContext(ponyWithMissingName);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(ponyWithMissingName, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your pony needs a name.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "Name");
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_with_a_name_exceeding_max_length()
        {
            const int ponyNameMaxLength = 36;

            var ponyWithNameExceedingMaxLength = new PonyModel
            {
                Name = Any.String(ponyNameMaxLength + 1),
                BodyColor = Any.String(),
                ManeColor = Any.String(),
                PonyType = Any.String()
            };

            var validationContext = new ValidationContext(ponyWithNameExceedingMaxLength);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(ponyWithNameExceedingMaxLength, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your pony's name can't be longer than 36 characters.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "Name");
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_without_body_color()
        {
            var ponyWithMissingBodyColor = new PonyModel
            {
                Name = Any.String(),
                ManeColor = Any.String(),
                PonyType = Any.String()
            };

            var validationContext = new ValidationContext(ponyWithMissingBodyColor);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(ponyWithMissingBodyColor, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your pony needs a body color.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "BodyColor");
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_with_a_body_color_exceeding_max_length()
        {
            const int bodyColorMaxLength = 20;

            var ponyWithBodyColorExceedingMaxLength = new PonyModel
            {
                Name = Any.String(),
                BodyColor = Any.String(bodyColorMaxLength + 1),
                ManeColor = Any.String(),
                PonyType = Any.String()
            };

            var validationContext = new ValidationContext(ponyWithBodyColorExceedingMaxLength);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(ponyWithBodyColorExceedingMaxLength, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your pony's body color can't be longer than 20 characters.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "BodyColor");
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_without_mane_color()
        {
            var ponyWithMissingManeColor = new PonyModel
            {
                Name = Any.String(),
                BodyColor = Any.String(),
                PonyType = Any.String()
            };

            var validationContext = new ValidationContext(ponyWithMissingManeColor);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(ponyWithMissingManeColor, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your pony needs a mane color.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "ManeColor");
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_with_a_mane_color_exceeding_max_length()
        {
            const int maneColorMaxLength = 20;

            var ponyWithManeColorExceedingMaxLength = new PonyModel
            {
                Name = Any.String(),
                BodyColor = Any.String(),
                ManeColor = Any.String(maneColorMaxLength + 1),
                PonyType = Any.String()
            };

            var validationContext = new ValidationContext(ponyWithManeColorExceedingMaxLength);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(ponyWithManeColorExceedingMaxLength, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your pony's mane color can't be longer than 20 characters.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "ManeColor");
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_without_pony_type()
        {
            var ponyWithMissingPonyType = new PonyModel
            {
                Name = Any.String(),
                ManeColor = Any.String(),
                BodyColor = Any.String()
            };

            var validationContext = new ValidationContext(ponyWithMissingPonyType);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(ponyWithMissingPonyType, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your pony needs a pony type.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "PonyType");
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_with_a_pony_type_exceeding_max_length()
        {
            const int ponyTypeMaxLength = 10;

            var ponyWithPonyTypExceedingMaxLength = new PonyModel
            {
                Name = Any.String(),
                ManeColor = Any.String(),
                BodyColor = Any.String(),
                PonyType = Any.String(ponyTypeMaxLength + 1)
            };

            var validationContext = new ValidationContext(ponyWithPonyTypExceedingMaxLength);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(ponyWithPonyTypExceedingMaxLength, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your pony's type can't be longer than 10 characters.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "PonyType");
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_with_pony_type_that_does_not_exist()
        {
            var ponyModelWithNonexistentPonyType = Any.PonyModel();

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color>
                    {
                        new Color { Name = ponyModelWithNonexistentPonyType.BodyColor },
                        new Color { Name = ponyModelWithNonexistentPonyType.ManeColor }
                    })
                .Build()
                .Object;

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType>())
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorTableStub)
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWork).Post(ponyModelWithNonexistentPonyType) as JsonResult;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal("Your pony's pony type doesn't exist!", response.Value.ToString());
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_with_body_color_that_does_not_exist()
        {
            var ponyWithNonexistentManeColor = Any.PonyModel();

            var colorsTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color>())
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorsTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWork).Post(ponyWithNonexistentManeColor) as JsonResult;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal("Your pony's body color doesn't exist!", response.Value.ToString());
        }

        [Fact]
        public void provide_validation_error_when_pony_is_posted_with_mane_color_that_does_not_exist()
        {
            var ponyWithNonexistentManeColor = Any.PonyModel();

            var colorsTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color> { new Color { Name = ponyWithNonexistentManeColor.BodyColor } })
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorsTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWork).Post(ponyWithNonexistentManeColor) as JsonResult;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal("Your pony's mane color doesn't exist!", response.Value.ToString());
        }

        [Fact]
        public void provide_validation_error_when_pony_is_updated_with_pony_type_that_does_not_exist()
        {
            var ponyModelWithNonexistentPonyType = Any.PonyModel();
            var ponyDbEntityWithNonexistentPonyType = Any.PonyDbEntity();

            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(new List<Pony> { ponyDbEntityWithNonexistentPonyType })
                .Build()
                .Object;

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color>
                    {
                        new Color { Name = ponyModelWithNonexistentPonyType.BodyColor },
                        new Color { Name = ponyModelWithNonexistentPonyType.ManeColor }
                    })
                .Build()
                .Object;

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType>())
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .WithColorTable(colorTableStub)
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWork).Put(ponyDbEntityWithNonexistentPonyType.Id, ponyModelWithNonexistentPonyType) as JsonResult;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal("Your pony's pony type doesn't exist!", response.Value.ToString());
        }

        [Fact]
        public void provide_validation_error_when_pony_is_updated_with_body_color_that_does_not_exist()
        {
            var ponyModelWithNonexistentBodyColor = Any.PonyModel();
            var ponyDbEntityWithNonexistentBodyColor = Any.PonyDbEntity();

            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(new List<Pony> { ponyDbEntityWithNonexistentBodyColor })
                .Build()
                .Object;

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color>())
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .WithColorTable(colorTableStub)
                .Build()
                .Object;
            
            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWork).Put(ponyDbEntityWithNonexistentBodyColor.Id, ponyModelWithNonexistentBodyColor) as JsonResult;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal("Your pony's body color doesn't exist!", response.Value.ToString());
        }

        [Fact]
        public void provide_validation_error_when_pony_is_updated_with_mane_color_that_does_not_exist()
        {
            var ponyModelWithNonexistentManeColor = Any.PonyModel();
            var ponyDbEntityWithNonexistentManeColor = Any.PonyDbEntity();

            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(new List<Pony> { ponyDbEntityWithNonexistentManeColor })
                .Build()
                .Object;

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color> { new Color { Name = ponyModelWithNonexistentManeColor.BodyColor } })
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub)
                .WithColorTable(colorTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PoniesController(unitOfWork).Put(ponyDbEntityWithNonexistentManeColor.Id, ponyModelWithNonexistentManeColor) as JsonResult;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal("Your pony's mane color doesn't exist!", response.Value.ToString());
        }
    }
}
