﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using PonyCollection.Controllers;
using PonyCollection.Entities;
using PonyCollection.Repositories;
using Xunit;

namespace PonyCollection.Tests.UnitTests
{
    public class ColorsController_should_
    {
        [Fact]
        public void retrieve_all_colors()
        {
            var expectedColors = Any.Colors(3);

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(expectedColors)
                .Build();

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorTableStub.Object)
                .Build();

            var unitOfWork = new UnitOfWork(databaseStub.Object);
            
            var response = new ColorsController(unitOfWork).Get() as JsonResult;
            var actualColors = response.Value as List<Color>;

            Assert.Equal((int)HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(expectedColors[0].Name, actualColors[0].Name);
            Assert.Equal(expectedColors[1].Name, actualColors[1].Name);
            Assert.Equal(expectedColors[2].Name, actualColors[2].Name);
        }

        [Fact]
        public void return_an_empty_list_if_no_colors_are_found_when_attempting_to_retrieve_all_colors()
        {
            var emptyListOfColors = new List<Color>();

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(emptyListOfColors)
                .Build();

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorTableStub.Object)
                .Build();

            var unitOfWork = new UnitOfWork(databaseStub.Object);

            var response = new ColorsController(unitOfWork).Get() as JsonResult;
            var actualColors = response.Value as List<Color>;

            Assert.Equal((int)HttpStatusCode.OK, response.StatusCode);
            Assert.Empty(actualColors);
        }

        [Fact]
        public void retrieve_one_color()
        {
            var expectedColor = Any.Color();

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color> { expectedColor })
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);
            
            var response = new ColorsController(unitOfWork).Get(expectedColor.Id) as JsonResult;
            var actualColor = response.Value as Color;

            Assert.Equal((int)HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(expectedColor.Id, actualColor.Id);
            Assert.Equal(expectedColor.Name, actualColor.Name);
        }

        [Fact]
        public void error_when_attempting_to_retrieve_color_that_does_not_exist()
        {
            var emptyListOfColors = new List<Color>();

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(emptyListOfColors)
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new ColorsController(unitOfWork).Get(Any.Int());

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public void delete_a_color()
        {
            var colorToDelete = Any.Color();

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color> { colorToDelete })
                .WithEntityToDelete(colorToDelete)
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);
            
            var response = new ColorsController(unitOfWork).Delete(colorToDelete.Id);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void error_when_attempting_to_delete_color_that_does_not_exist()
        {
            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color>())
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new ColorsController(unitOfWork).Delete(Any.Int());

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public void create_a_color()
        {
            var expectedColor = new Color { Name = Any.String() };

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(new Mock<DbSet<Color>>().Object)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new ColorsController(unitOfWork).Post(expectedColor) as JsonResult;
            var actualColor = response.Value as Color;

            Assert.Equal((int)HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(expectedColor.Name, actualColor.Name);
        }

        [Fact]
        public void error_when_attempting_to_create_a_color_that_already_exists()
        {
            var colorToCreate = new Color { Name = Any.String() };
            var expectedColor = new Color { Id = Any.Int(), Name = colorToCreate.Name };
            var duplicateEntryExceptionMessage = $"Duplicate entry '{colorToCreate.Name}' for key 'uc_Name'";
            var colorAlreadyExistsException = new DbUpdateException(duplicateEntryExceptionMessage, new Exception(duplicateEntryExceptionMessage));

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(new Mock<DbSet<Color>>().Object)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWorkStubBuilder()
                .WithPonyCollectionDbContext(databaseStub)
                .ThatThrowsExceptionWhenCompletingTransaction(colorAlreadyExistsException)
                .Build();

            var response = new ColorsController(unitOfWork).Post(colorToCreate) as JsonResult;
            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);

            var expectedErrorMessage = "Oops! That color already exists.";
            var actualErrorMessage = response.Value.ToString();
            Assert.Equal(expectedErrorMessage, actualErrorMessage);
        }

        [Fact]
        public void update_a_color()
        {
            var colorToUpdate = Any.Color();
            var existingColor = new Color
            {
                Id = colorToUpdate.Id,
                Name = Any.String()
            };

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color> { existingColor })
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorTableStub)
                .Build()
                .Object;

            var unitOfWorkStub = new UnitOfWork(databaseStub);

            var response = new ColorsController(unitOfWorkStub).Put(colorToUpdate.Id, colorToUpdate) as JsonResult;
            var actualColor = response.Value as Color;

            Assert.Equal((int)HttpStatusCode.Accepted, response.StatusCode);
            Assert.Equal(colorToUpdate.Name, actualColor.Name);
        }

        [Fact]
        public void error_when_attempting_to_update_color_that_does_not_exist()
        {
            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color>())
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);
            
            var response = new ColorsController(unitOfWork).Put(Any.Int(), Any.Color());

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public void error_when_attempting_to_update_a_color_to_a_value_that_already_exists()
        {
            var colorToUpdate = Any.Color();
            var duplicateEntryExceptionMessage = $"Duplicate entry '{colorToUpdate.Name}' for key 'uc_Name'";
            var colorAlreadyExistsException = new DbUpdateException(duplicateEntryExceptionMessage, new Exception(duplicateEntryExceptionMessage));

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color> { colorToUpdate })
                .Build()
                .Object;
            
            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithColorTable(colorTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWorkStubBuilder()
                .WithPonyCollectionDbContext(databaseStub)
                .ThatThrowsExceptionWhenCompletingTransaction(colorAlreadyExistsException)
                .Build();

            var response = new ColorsController(unitOfWork).Put(colorToUpdate.Id, colorToUpdate) as JsonResult;
            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);

            var expectedErrorMessage = "Oops! That color already exists.";
            var actualErrorMessage = response.Value.ToString();
            Assert.Equal(expectedErrorMessage, actualErrorMessage);
        }

        [Fact]
        public void return_bad_request_error_when_new_color_is_invalid_for_any_reason()
        {
            var expectedErrorKey = Any.String();
            var expectedErrorMessage = Any.String();

            var unitOfWork = new UnitOfWorkStubBuilder().Build();
            var controller = new ColorsController(unitOfWork);
            controller.ModelState.AddModelError(expectedErrorKey, expectedErrorMessage);

            var response = controller.Post(null) as BadRequestObjectResult;
            var errors = response.Value as SerializableError;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal(expectedErrorKey, errors.Keys.First());
            Assert.Equal(new[] { expectedErrorMessage }, errors.Values.First());
        }

        [Fact]
        public void return_bad_request_error_when_updated_color_is_invalid_for_any_reason()
        {
			var expectedErrorKey = Any.String();
			var expectedErrorMessage = Any.String();

            var unitOfWork = new UnitOfWorkStubBuilder().Build();
            var controller = new ColorsController(unitOfWork);
            controller.ModelState.AddModelError(expectedErrorKey, expectedErrorMessage);

            var response = controller.Put(Any.Int(), null) as BadRequestObjectResult;
            var errors = response.Value as SerializableError;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal(expectedErrorKey, errors.Keys.First());
            Assert.Equal(new[] { expectedErrorMessage }, errors.Values.First());
        }

        [Fact]
        public void provide_validation_error_when_color_is_posted_without_name()
        {
            var colorWithMissingName = new Color();

            var validationContext = new ValidationContext(colorWithMissingName);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(colorWithMissingName, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your color needs a name.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "Name");
        }

        [Fact]
        public void provide_validation_error_when_color_is_posted_with_name_exceeding_max_length()
        {
            const int colorNameMaxLength = 20;
            var colorWithNameExceedingMaximumLength = new Color { Name = Any.String(colorNameMaxLength + 1) };

            var validationContext = new ValidationContext(colorWithNameExceedingMaximumLength);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(colorWithNameExceedingMaximumLength, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your color's name can't be longer than 20 characters.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "Name");
        }
    }
}
