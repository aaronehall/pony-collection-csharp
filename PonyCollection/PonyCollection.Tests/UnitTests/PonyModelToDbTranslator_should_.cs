﻿using System.Collections.Generic;
using PonyCollection.Entities;
using PonyCollection.Models;
using PonyCollection.Repositories;
using Xunit;

namespace PonyCollection.Tests.UnitTests
{
    public class PonyModelToDbTranslator_should_
    {
        [Fact]
        public void translate_a_pony_db_entity_to_a_model()
        {
            var expectedPonyType = Any.PonyType();
            var expectedBodyColor = Any.Color();
            var expectedManeColor = Any.Color();

            var ponyDbEntity = new Pony
            {
                Id = Any.Int(),
                Name = Any.String(),
                PonyTypeId = expectedPonyType.Id,
                BodyColorId = expectedBodyColor.Id,
                ManeColorId = expectedManeColor.Id,
                PonyType = expectedPonyType,
                BodyColor = expectedBodyColor,
                ManeColor = expectedManeColor
            };

            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(new List<Pony> { ponyDbEntity })
                .Build();

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub.Object)
                .Build();

            var actualPonyModel = new PonyModelToDbTranslator(databaseStub.Object).GetModelFromDbEntity(ponyDbEntity);

            Assert.Equal(ponyDbEntity.Id, actualPonyModel.Id);
            Assert.Equal(ponyDbEntity.Name, actualPonyModel.Name);
            Assert.Equal(expectedPonyType.Name, actualPonyModel.PonyType);
            Assert.Equal(expectedBodyColor.Name, actualPonyModel.BodyColor);
            Assert.Equal(expectedManeColor.Name, actualPonyModel.ManeColor);
        }

        [Fact]
        public void translate_multiple_pony_db_entities_to_models()
        {
            var expectedPonyType = Any.PonyType();
            var expectedBodyColor = Any.Color();
            var expectedManeColor = Any.Color();

            var ponyDbEntity1 = new Pony
            {
                Id = Any.Int(),
                Name = Any.String(),
                PonyTypeId = expectedPonyType.Id,
                BodyColorId = expectedBodyColor.Id,
                ManeColorId = expectedManeColor.Id,
                PonyType = expectedPonyType,
                BodyColor = expectedBodyColor,
                ManeColor = expectedManeColor
            };

            var ponyDbEntity2 = new Pony
            {
                Id = Any.Int(),
                Name = Any.String(),
                PonyTypeId = expectedPonyType.Id,
                BodyColorId = expectedBodyColor.Id,
                ManeColorId = expectedManeColor.Id,
                PonyType = expectedPonyType,
                BodyColor = expectedBodyColor,
                ManeColor = expectedManeColor
            };

            var ponyDbEntities = new List<Pony> { ponyDbEntity1, ponyDbEntity2 };

            var ponyTableStub = new TableMockBuilder<Pony>()
                .WithExistingEntitiesToSelect(ponyDbEntities)
                .Build();
            
            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTable(ponyTableStub.Object)
                .Build();

            var actualPonyModels = new PonyModelToDbTranslator(databaseStub.Object).GetModelsFromDbEntities(ponyDbEntities);

            Assert.Equal(ponyDbEntity1.Id, actualPonyModels[0].Id);
            Assert.Equal(ponyDbEntity1.Name, actualPonyModels[0].Name);
            Assert.Equal(ponyDbEntity1.PonyType.Name, actualPonyModels[0].PonyType);
            Assert.Equal(ponyDbEntity1.BodyColor.Name, actualPonyModels[0].BodyColor);
            Assert.Equal(ponyDbEntity1.ManeColor.Name, actualPonyModels[0].ManeColor);

            Assert.Equal(ponyDbEntity2.Id, actualPonyModels[1].Id);
            Assert.Equal(ponyDbEntity2.Name, actualPonyModels[1].Name);
            Assert.Equal(ponyDbEntity2.PonyType.Name, actualPonyModels[1].PonyType);
            Assert.Equal(ponyDbEntity2.BodyColor.Name, actualPonyModels[1].BodyColor);
            Assert.Equal(ponyDbEntity2.ManeColor.Name, actualPonyModels[1].ManeColor);
        }

        [Fact]
        public void translate_pony_model_to_db_entity()
        {
            var expectedPonyType = Any.PonyType();
            var expectedBodyColor = Any.Color();
            var expectedManeColor = Any.Color();

            var ponyModel = new PonyModel
            {
                Id = Any.Int(),
                Name = Any.String(),
                PonyType = expectedPonyType.Name,
                BodyColor = expectedBodyColor.Name,
                ManeColor = expectedManeColor.Name
            };

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType> { expectedPonyType })
                .Build();

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color> { expectedBodyColor, expectedManeColor })
                .Build();

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub.Object)
                .WithColorTable(colorTableStub.Object)
                .Build();

            var actualDbEntity = new PonyModelToDbTranslator(databaseStub.Object).GetDbEntityFromModel(ponyModel);

            Assert.Equal(expectedPonyType.Id, actualDbEntity.PonyTypeId);
            Assert.Equal(expectedBodyColor.Id, actualDbEntity.BodyColorId);
            Assert.Equal(expectedManeColor.Id, actualDbEntity.ManeColorId);
        }
    }
}
