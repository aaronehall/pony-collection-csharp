﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Newtonsoft.Json;
using PonyCollection.Controllers;
using PonyCollection.Entities;
using PonyCollection.Repositories;
using Xunit;

namespace PonyCollection.Tests.UnitTests
{
    public class PonyTypesController_should_
    {
        [Fact]
        public void retrieve_all_pony_types()
        {
            var expectedPonyTypes = Any.PonyTypes(3);

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                 .WithExistingEntitiesToSelect(expectedPonyTypes)
                 .Build()
                 .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);
            
            var response = new PonyTypesController(unitOfWork).Get() as JsonResult;
            var actualPonyTypes = response.Value as List<PonyType>;

            Assert.Equal((int)HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(expectedPonyTypes[0].Name, actualPonyTypes[0].Name);
            Assert.Equal(expectedPonyTypes[1].Name, actualPonyTypes[1].Name);
            Assert.Equal(expectedPonyTypes[2].Name, actualPonyTypes[2].Name);
        }

        [Fact]
        public void return_an_empty_list_if_no_pony_types_are_found_when_attempting_to_retrieve_all_pony_types()
        {
            var emptyListOfPonyTypes = new List<PonyType>();

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                 .WithExistingEntitiesToSelect(emptyListOfPonyTypes)
                 .Build()
                 .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PonyTypesController(unitOfWork).Get() as JsonResult;
            var actualPonyTypes = response.Value as List<PonyType>;

            Assert.Equal((int)HttpStatusCode.OK, response.StatusCode);
            Assert.Empty(actualPonyTypes);
        }

        [Fact]
        public void retrieve_one_pony_type()
        {
            var expectedPonyType = Any.PonyType();

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType> { expectedPonyType })
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PonyTypesController(unitOfWork).Get(expectedPonyType.Id) as JsonResult;
            var actualPonyType = response.Value as PonyType;

            Assert.Equal((int)HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(expectedPonyType, actualPonyType);
        }

        [Fact]
        public void error_when_attempting_to_retrieve_pony_type_that_does_not_exist()
        {
            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType>())
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PonyTypesController(unitOfWork).Get(Any.Int());

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public void delete_a_pony_type()
        {
            var ponyTypeToDelete = Any.PonyType();

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType> { ponyTypeToDelete })
                .WithEntityToDelete(ponyTypeToDelete)
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;
            
            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PonyTypesController(unitOfWork).Delete(ponyTypeToDelete.Id);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void error_when_attempting_to_delete_a_pony_type_that_does_not_exist()
        {
            var ponyTypeToDelete = Any.PonyType();

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType> { ponyTypeToDelete })
                .WithEntityToDelete(ponyTypeToDelete)
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PonyTypesController(unitOfWork).Delete(Any.Int());

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public void create_a_pony_type()
        {
            var expectedPonyType = new PonyType { Name = Any.String() };

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PonyTypesController(unitOfWork).Post(expectedPonyType) as JsonResult;
            var actualPonyType = response.Value as PonyType;

            Assert.Equal((int)HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(expectedPonyType.Name, actualPonyType.Name);
        }

        [Fact]
        public void error_when_attempting_to_create_a_pony_type_that_already_exists()
        {
            var ponyTypeToCreate = new PonyType { Name = Any.String() };
            var duplicateEntryExceptionMessage = $"Duplicate entry '{ponyTypeToCreate.Name}' for key 'uc_Name'";
            var ponyTypeAlreadyExistsException = new DbUpdateException(duplicateEntryExceptionMessage, new Exception(duplicateEntryExceptionMessage));

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(new Mock<DbSet<PonyType>>().Object)
                .Build()
                .Object;

            var unitOfWorkStub = new UnitOfWorkStubBuilder()
                .WithPonyCollectionDbContext(databaseStub)
                .ThatThrowsExceptionWhenCompletingTransaction(ponyTypeAlreadyExistsException)
                .Build();

            var response = new PonyTypesController(unitOfWorkStub).Post(ponyTypeToCreate) as JsonResult;
            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);

            var expectedErrorMessage = "Oops! That pony type already exists.";
            var actualErrorMessage = response.Value.ToString();
            Assert.Equal(expectedErrorMessage, actualErrorMessage);
        }

        [Fact]
        public void update_a_pony_type()
        {
            var existingPonyType = Any.PonyType();
            var updatedPonyType = new PonyType { Id = existingPonyType.Id, Name = Any.String() };

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType> { existingPonyType })
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;
            
            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PonyTypesController(unitOfWork).Put(existingPonyType.Id, updatedPonyType) as JsonResult;
            var actualPonyType = response.Value as PonyType;

            Assert.Equal((int)HttpStatusCode.Accepted, response.StatusCode);
            Assert.Equal(updatedPonyType.Id, actualPonyType.Id);
            Assert.Equal(updatedPonyType.Name, actualPonyType.Name);
        }

        [Fact]
        public void error_when_attempting_to_update_a_pony_type_that_does_not_exist()
        {
            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType>())
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWork = new UnitOfWork(databaseStub);

            var response = new PonyTypesController(unitOfWork).Put(Any.Int(), Any.PonyType());

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public void error_when_attempting_to_update_a_pony_type_to_a_value_that_already_exists()
        {
            var existingPonyType = Any.PonyType();
            var ponyTypeToEdit = new PonyType { Id = existingPonyType.Id, Name = Any.String() };
            var duplicateEntryExceptionMessage = $"Duplicate entry '{ponyTypeToEdit.Name}' for key 'uc_Name'";
            var ponyTypeAlreadyExistsException = new DbUpdateException(duplicateEntryExceptionMessage, new Exception(duplicateEntryExceptionMessage));

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType> { existingPonyType })
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWorkStub = new UnitOfWorkStubBuilder()
                .WithPonyCollectionDbContext(databaseStub)
                .ThatThrowsExceptionWhenCompletingTransaction(ponyTypeAlreadyExistsException)
                .Build();

            var response = new PonyTypesController(unitOfWorkStub).Put(ponyTypeToEdit.Id, ponyTypeToEdit) as JsonResult;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);

            var expectedErrorMessage = "Oops! That pony type already exists.";
            var actualErrorMessage = response.Value.ToString();
            Assert.Equal(expectedErrorMessage, actualErrorMessage);
        }

        [Fact]
        public void return_bad_request_error_when_new_pony_type_is_invalid_for_any_reason()
        {
            var expectedErrorKey = Any.String();
            var expectedErrorMessage = Any.String();

            var unitOfWork = new UnitOfWorkStubBuilder().Build();
            var controller = new PonyTypesController(unitOfWork);
            controller.ModelState.AddModelError(expectedErrorKey, expectedErrorMessage);

            var response = controller.Post(null) as BadRequestObjectResult;
            var errors = response.Value as SerializableError;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal(expectedErrorKey, errors.Keys.First());
            Assert.Equal(new[] { expectedErrorMessage }, errors.Values.First());
        }

        [Fact]
        public void return_bad_request_error_when_updated_pony_type_is_invalid_for_any_reason()
        {
            var expectedErrorKey = Any.String();
            var expectedErrorMessage = Any.String();

            var unitOfWork = new UnitOfWorkStubBuilder().Build();
            var controller = new PonyTypesController(unitOfWork);
            controller.ModelState.AddModelError(expectedErrorKey, expectedErrorMessage);

            var response = controller.Put(Any.Int(), null) as BadRequestObjectResult;
            var errors = response.Value as SerializableError;

            Assert.Equal((int)HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal(expectedErrorKey, errors.Keys.First());
            Assert.Equal(new[] { expectedErrorMessage }, errors.Values.First());
        }

        [Fact]
        public void provide_validation_error_when_pony_type_is_posted_with_a_name_exceeding_max_length()
        {
            const int ponyTypeNameMaxLength = 10;
            var ponyTypeWithNameExceedingMaxLength = new PonyType { Name = Any.String(ponyTypeNameMaxLength + 1) };

            var validationContext = new ValidationContext(ponyTypeWithNameExceedingMaxLength);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(ponyTypeWithNameExceedingMaxLength, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your pony type's name can't be longer than 10 characters.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "Name");
        }

        [Fact]
        public void provide_validation_error_when_pony_type_is_posted_without_a_name()
        {
            var ponyTypeWithMissingName = new PonyType { Name = string.Empty };

            var validationContext = new ValidationContext(ponyTypeWithMissingName);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(ponyTypeWithMissingName, validationContext, validationResults, true);

            Assert.False(isValid);
            var validationFailure = Assert.Single(validationResults, validationResult => validationResult.ErrorMessage == "Your pony type needs a name.");
            Assert.Single(validationFailure.MemberNames, fieldName => fieldName == "Name");
        }

        [Fact]
        public void not_return_ponies_property_on_pony_types_object()
        {
            var ponyEntity = Any.PonyDbEntity();

            var expectedPonyType = new PonyType
            {
                Id = Any.Int(),
                Name = Any.String(),
                Ponies = new List<Pony>
                {
                    ponyEntity
                }
            };

            var ponyTypeTableStub = new TableMockBuilder<PonyType>()
                .WithExistingEntitiesToSelect(new List<PonyType> { expectedPonyType })
                .Build()
                .Object;

            var databaseStub = new PonyCollectionDatabaseMockBuilder()
                .WithPonyTypeTable(ponyTypeTableStub)
                .Build()
                .Object;

            var unitOfWorkStub = new UnitOfWorkStubBuilder()
                .WithPonyCollectionDbContext(databaseStub)
                .Build();

            var response = new PonyTypesController(unitOfWorkStub).Get(expectedPonyType.Id) as JsonResult;
            var actualPonyTypeAsJsonString = JsonConvert.SerializeObject(response.Value);

            Assert.DoesNotContain(ponyEntity.Name, actualPonyTypeAsJsonString);
        }
    }
}
