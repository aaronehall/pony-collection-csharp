﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using PonyCollection.Entities;
using PonyCollection.Repositories;
using Xunit;

namespace PonyCollection.Tests.UnitTests
{
    public class Repository_should_
    {
        [Fact]
        public void delete_one_entity()
        {
            var colorToDelete = Any.Color();

            var ponyCollectionDbStub = new Mock<PonyCollectionDbContext>();
            ponyCollectionDbStub.Setup(db => db.Set<Color>().Remove(colorToDelete));

            new RepositoryBase<Color>(ponyCollectionDbStub.Object).Delete(colorToDelete);

            ponyCollectionDbStub.Verify(stub => stub.Set<Color>().Remove(colorToDelete), Times.Once);
        }

        [Fact]
        public void get_one_entity()
        {
            var expectedEntity = Any.Color();

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(new List<Color> { expectedEntity })
                .Build();

            var ponyCollectionDatabaseStub = new Mock<PonyCollectionDbContext>();
            ponyCollectionDatabaseStub.Setup(db => db.Set<Color>()).Returns(colorTableStub.Object);

            var actualEntity = new RepositoryBase<Color>(ponyCollectionDatabaseStub.Object).Get(entity => entity.Id == expectedEntity.Id);

            Assert.Equal(expectedEntity.Name, actualEntity.Name);
        }

        [Fact]
        public void get_all_entities()
        {
            var expectedEntities = Any.Colors(3);

            var colorTableStub = new TableMockBuilder<Color>()
                .WithExistingEntitiesToSelect(expectedEntities)
                .Build();

            var ponyCollectionDatabaseStub = new Mock<PonyCollectionDbContext>();
            ponyCollectionDatabaseStub.Setup(db => db.Set<Color>()).Returns(colorTableStub.Object);

            var actualEntities = new RepositoryBase<Color>(ponyCollectionDatabaseStub.Object).GetAll();

            Assert.Equal(expectedEntities.ElementAt(0).Id, actualEntities.ElementAt(0).Id);
            Assert.Equal(expectedEntities.ElementAt(0).Name, actualEntities.ElementAt(0).Name);

            Assert.Equal(expectedEntities.ElementAt(1).Id, actualEntities.ElementAt(1).Id);
            Assert.Equal(expectedEntities.ElementAt(1).Name, actualEntities.ElementAt(1).Name);

            Assert.Equal(expectedEntities.ElementAt(2).Id, actualEntities.ElementAt(2).Id);
            Assert.Equal(expectedEntities.ElementAt(2).Name, actualEntities.ElementAt(2).Name);
        }

        [Fact]
        public void add_an_entity()
        {
            var entityToCreate = new Color { Name = Any.String() };

            var colorTableMock = new TableMockBuilder<Color>()
                .Build();

            var ponyCollectionDatabaseMock = new Mock<PonyCollectionDbContext>();
            ponyCollectionDatabaseMock.Setup(db => db.Set<Color>().Add(entityToCreate));

            new RepositoryBase<Color>(ponyCollectionDatabaseMock.Object).Add(entityToCreate);

            ponyCollectionDatabaseMock.Verify(db => db.Set<Color>().Add(entityToCreate), Times.Once);
        }
    }
}
