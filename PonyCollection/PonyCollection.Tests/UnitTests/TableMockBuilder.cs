﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Moq;
using PonyCollection.Entities;

namespace PonyCollection.Tests.UnitTests
{
    public class TableMockBuilder<T> where T : EntityBase
    {
        private readonly Mock<DbSet<T>> _tableMock = new Mock<DbSet<T>>();
        private List<T> _existingEntitiesToSelect;
        private T _entityToDelete;

        public TableMockBuilder<T> WithExistingEntitiesToSelect(List<T> existingEntitiesToSelect)
        {
            _existingEntitiesToSelect = existingEntitiesToSelect;
            return this;
        }

        public TableMockBuilder<T> WithEntityToDelete(T entityToDelete)
        {
            _entityToDelete = entityToDelete;
            return this;
        }

        public Mock<DbSet<T>> Build()
        {
            if (_existingEntitiesToSelect != null)
            {
                var queryableEntities = _existingEntitiesToSelect.AsQueryable();

                _tableMock.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryableEntities.Provider);
                _tableMock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryableEntities.Expression);
                _tableMock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryableEntities.ElementType);
                _tableMock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(queryableEntities.GetEnumerator());
            }

            if (_entityToDelete != null)
            {
                _tableMock.Setup(table => table.Remove(It.Is<T>(entity => entity.Id == _entityToDelete.Id)));
            }

            return _tableMock;
        }
    }
}
