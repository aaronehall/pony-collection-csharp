﻿using System;
using Moq;
using PonyCollection.Entities;
using PonyCollection.Repositories;

namespace PonyCollection.Tests.UnitTests
{
    public class UnitOfWorkStubBuilder
    {
        private Mock<UnitOfWork> _unitOfWorkStub;
        private PonyCollectionDbContext _dbContext;
        private Exception _exception;

        public UnitOfWorkStubBuilder WithPonyCollectionDbContext(PonyCollectionDbContext dbContext)
        {
            _dbContext = dbContext;
            return this;
        }

        public UnitOfWorkStubBuilder ThatThrowsExceptionWhenCompletingTransaction(Exception exception)
        {
            _exception = exception;
            return this;
        }

        public UnitOfWork Build()
        {
            _unitOfWorkStub = new Mock<UnitOfWork>(_dbContext);

            if (_exception != null)
            {
                _unitOfWorkStub.Setup(stub => stub.Complete()).Throws(_exception);
            }
            else
            {
                _unitOfWorkStub.Setup(_stub => _stub.Complete());
            }

            return _unitOfWorkStub.Object;
        }
    }
}
