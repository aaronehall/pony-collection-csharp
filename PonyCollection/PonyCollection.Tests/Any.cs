﻿using System;
using System.Collections.Generic;
using System.Text;
using PonyCollection.Entities;
using PonyCollection.Models;

namespace PonyCollection.Tests
{
    public static class Any
    {
        private static readonly Random _random = new Random();

        public static string String(int length = 10)
        {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringBuilder = new StringBuilder();

            for (var i = 0; i < length; i++)
            {
                stringBuilder.Append(chars[_random.Next(chars.Length)]);
            }

            return stringBuilder.ToString();
        }

        public static int Int(int max = 9999) => _random.Next(max);

        public static Pony PonyDbEntity()
        {
            var bodyColorId = Int();
            var maneColorId = bodyColorId - 1;
            var ponyTypeId = Int();

            return new Pony
            {
                Id = Int(),
                Name = String(),
                BodyColorId = bodyColorId,
                ManeColorId = maneColorId,
                PonyTypeId = ponyTypeId,
                BodyColor = new Color { Id = bodyColorId, Name = String() },
                ManeColor = new Color { Id = maneColorId, Name = String() },
                PonyType = new PonyType { Id = ponyTypeId, Name = String() }
            };
        }

        public static List<Pony> PonyDbEntities(int length = 10)
        {
            var ponyEntities = new List<Pony>();

            for (var i = 0; i < length; i++)
            {
                ponyEntities.Add(PonyDbEntity());
            }

            return ponyEntities;
        }

        public static PonyModel PonyModel(bool includeId = true)
        {
            var pony = new PonyModel
            {
                Name = String(),
                BodyColor = String(),
                ManeColor = String(),
                PonyType = String()
            };

            if (includeId)
            {
                pony.Id = Int(); 
            }

            return pony;
        }

        public static List<PonyModel> PonyModels(int length = 10)
        {
            var ponyModels = new List<PonyModel>();

            for (var i = 0; i < length; i++)
            {
                ponyModels.Add(PonyModel());
            }

            return ponyModels;
        }

        public static Color Color()
        {
            return new Color
            {
                Id = Int(),
                Name = String()
            };
        }

        public static List<Color> Colors(int length = 10)
        {
            var colors = new List<Color>();

            for (var i = 0; i < length; i++)
            {
                colors.Add(Color());
            }

            return colors;
        }

        public static PonyType PonyType()
        {
            return new PonyType
            {
                Id = Int(),
                Name = String()
            };
        }

        public static List<PonyType> PonyTypes(int length = 10)
        {
            var ponyTypes = new List<PonyType>();

            for (var i = 0; i < length; i++)
            {
                ponyTypes.Add(PonyType());
            }

            return ponyTypes;
        }
    }
}
